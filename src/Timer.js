import React from 'react';

class Timer extends React.Component {
  constructor() {
    super();
		this.state = {
			timerValue: 0
		};

		this.timer = null;
		this.updateTimer = this.updateTimer.bind(this);
  }

	componentDidUpdate(prevProps) {
		if (prevProps.timerStarted !== this.props.timerStarted) {
			if (this.props.timerStarted) {
				this.timer = setInterval(this.updateTimer, 1000);
			} else {
				clearInterval(this.timer);
				this.timer = null;
				this.setState({ timerValue: 0 });
			}
		}
	}

	componentWillUnmount() {
		clearInterval(this.timer);
		this.timer = null;
		alert('Timer unmounted');
	}

	updateTimer() {
		this.setState({ timerValue: this.state.timerValue + 1 });
	}

  render() {
    return (
      this.props.timerStarted && <div>Value: {this.state.timerValue}</div>
    )
  }
}

export default Timer;
